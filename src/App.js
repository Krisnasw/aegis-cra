import React from 'react';
import Banner from './components/banner'
import BannerAegis from './components/banner-aegis'
import Brand from './components/brand'
import FeatureCoach from './components/feature-coach'
import Testimonials from './components/testimonials'
import Footer from './components/footer'

function App() {
  return (
    <div>
      <Banner />
      <BannerAegis />
      <FeatureCoach />
      <Brand />
      <Testimonials />
      <Footer />
    </div>
  );
}

export default App;
