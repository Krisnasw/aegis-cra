import React from 'react';
import styled from 'styled-components';
import { Avatar } from 'antd';

const AvaStyled = styled(Avatar)`
  width: ${props => (props.size && props.size) || 20}px !important;
  height: ${props => (props.size && props.size) || 20}px !important;
  border-radius: ${props => (props.radius && props.radius) || 0}% !important;
`;

export class KaoAvatar extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      image: props.image || '',
    };
  }

  render() {
    const {
      title, shape, size, radius, icon,
    } = this.props;
    return (
      <div>
        {title ? (
          <AvaStyled icon={icon} shape={shape} size={size} radius={radius}>
            {title}
          </AvaStyled>
        ) : (
          <AvaStyled
            icon={icon}
            shape={shape}
            size={size}
            radius={radius}
            src={this.state.image}
            onMouseOver={this.mouseOver}
            onMouseOut={this.mouseOut}
          />
        )}
      </div>
    );
  }

  mouseOut = () => {
    this.setState({
      image: this.props.image,
    });
  };

  mouseOver = () => {
    const { hoverImage, image } = this.props;
    this.setState({
      image: (this.props.hoverImage && hoverImage) || image,
    });
  };
}
