import React from 'react';
import styled from 'styled-components';
import { Button } from 'antd';

export const btn = ({ children, ...rest }) => <Button {...rest}>{children}</Button>;

export const KaoButton = styled(btn)`
  border-radius: 20px !important;
  padding: 0 10px !important;
  margin: 0;
  height: 40px !important;
  width: ${props => (props.width || 40) / 16}rem;
`;

export const KaoButtonLink = styled(KaoButton)`
  border: none !important;
`;
