import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import DiscordLogo from '../assets/images/discord-logo.svg';
import { btn } from './common';

export const KaoSocialBtn = styled(btn)`
  min-width: 100px;
  border-radius: 5px !important;
  cursor: pointer;
  color: rgba(54, 58, 69, 0.87);
  font-size: 16px;
  font-weight: 400;
  line-height: 21px;
  padding: 0.25em 1em;
  margin: 0;
  height: 70px !important;
  width: ${props => (props.width || 40) / 16}rem;
  width: 230px;
`;

const DiscordBtn = styled(KaoSocialBtn)`
  background: #23272a;
`;

export class SocialLogin extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node,
    onFailure: PropTypes.func,
    onRequest: PropTypes.func,
    onSuccess: PropTypes.func,
    scope: PropTypes.string,
    text: PropTypes.string,
    type: PropTypes.oneOf(['discord']),
  };

  static defaultProps = {
    children: null,
    onFailure: () => {},
    onRequest: () => {},
    onSuccess: () => {},
    scope: 'profile email',
    text: '',
    type: 'discord',
  };

  render() {
    const {
      clientId, onFailure, onRequest, onSuccess, scope, type, text, ...rest
    } = this.props;

    switch (type) {
      case 'discord':
        return (
          <DiscordBtn
            onFailure={onFailure}
            onRequest={onRequest}
            onSuccess={onSuccess}
            scope={scope}
          >
            <DiscordLogo />
          </DiscordBtn>
        );
      default:
    }
  }
}
