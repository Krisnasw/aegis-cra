import styled from 'styled-components';

export const KaoCardSlide = styled.div`
  position: relative;
  overflow: hidden;
  padding: 0 !important;
  .cover {
    position: relative;
    height: 100%;
    -webkit-transition-property: -webkit-transform;
    transition-property: -webkit-transform;
    transition-property: transform;
    transition-property: transform, -webkit-transform;
    -webkit-transition-duration: 0.4s;
    transition-duration: 0.4s;
  }
  .caption {
    position: absolute;
    top: auto;
    bottom: 0;
    opacity: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: #ffffff;
    padding: 45px 35px;
    -webkit-transform: translateY(100%);
    transform: translateY(100%);
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    -webkit-transition: opacity 0.1s 0.3s, -webkit-transform 0.4s;
    transition: opacity 0.1s 0.3s, -webkit-transform 0.4s;
    transition: transform 0.4s, opacity 0.1s 0.3s;
    transition: transform 0.4s, opacity 0.1s 0.3s, -webkit-transform 0.4s;
  }
  &:hover .cover {
    -webkit-transform: translateY(-100%);
    transform: translateY(-100%);
  }
  &:hover .caption {
    opacity: 1;
    -webkit-transform: translateY(0px);
    transform: translateY(0px);
    -webkit-transition: opacity 0.1s, -webkit-transform 0.4s;
    transition: opacity 0.1s, -webkit-transform 0.4s;
    transition: transform 0.4s, opacity 0.1s;
    transition: transform 0.4s, opacity 0.1s, -webkit-transform 0.4s;
  }
  .action {
    position: absolute;
    bottom: 55px;
  }
`;
