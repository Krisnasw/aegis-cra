import React from 'react';
import styled from 'styled-components';
import { Col } from 'antd';

const col = ({ children, ...rest }) => <Col {...rest}>{children}</Col>;

export const KaoCol = styled(col)`
  && {
  }
`;
