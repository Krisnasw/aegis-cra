import React from 'react';
import styled from 'styled-components';
import { Row } from 'antd';

const flex = ({ children, ...rest }) => (
  <Row type="flex" {...rest}>
    {children}
  </Row>
);

export const KaoFlex = styled(flex)`
  && {
  }
`;

const row = ({ children, ...rest }) => <Row {...rest}>{children}</Row>;

export const KaoRow = styled(row)`
  && {
  }
`;
