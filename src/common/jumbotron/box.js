import React from 'react';
import styled from 'styled-components';
import { KaoRow, KaoCol } from '../grid';
import { KaoJumbotron } from './common';

const GrayScale = styled(KaoJumbotron)`
  background-attachment: inherit !important;
  height: inherit !important;
  filter: grayscale(100%) contrast(1.2);
  filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");
  -moz-filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");
  -o-filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");
  -webkit-filter: grayscale(100%) contrast(1);
  -webkit-transition: all ease-in-out 0.3s;
  transition: all ease-in-out 0.3s;
  &:before {
    content: '';
    padding-top: 100%;
    display: block;
  }
  &:hover {
    filter: none !important;
    -moz-filter: none !important;
    -o-filter: none !important;
    -webkit-filter: none !important;
  }
`;

const GridBoxComp = ({
  className, children, image, size,
}) => (
  <KaoRow>
    <KaoCol className={className}>
      <div className="container">
        <GrayScale image={image} />
      </div>
    </KaoCol>
  </KaoRow>
);

export const GridBox = styled(GridBoxComp)`
  .container {
    witdh: 100%;
    height: 100%;
    display: block;
    z-index: 1;
    margin-top: 5px;
    margin-bottom: 5px;
  }
`;
