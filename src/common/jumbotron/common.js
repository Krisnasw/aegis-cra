import styled from 'styled-components';

export const KaoJumbotron = styled.div`
  background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)),
    url(${props => props.image && props.image});
  background-size: cover;
  background-position: center center;
  background-attachment: fixed;
  background-repeat: no-repeat;
  color: white;
  height: 100vh;
  position: relative;
  margin-bottom: 0;
`;
