import React from 'react';
import styled from 'styled-components';
import { Layout } from 'antd';

const layout = ({ children, ...rest }) => <Layout {...rest}>{children}</Layout>;

export const KaoLayout = styled(layout)``;
