import React from 'react';
import styled, { css } from 'styled-components';
import { Layout } from 'antd';

const { Content } = Layout;
const content = ({ children, ...rest }) => <Content {...rest}>{children}</Content>;

export const KaoContent = styled(content)``;
