import React from 'react';
import styled from 'styled-components';
import { Layout } from 'antd';

const { Footer } = Layout;

const footer = ({ children, ...rest }) => <Footer {...rest}>{children}</Footer>;

export const KaoFooter = styled(footer)``;
