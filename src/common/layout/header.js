import { Layout } from 'antd';
import { KaoMedia } from '../grid';
import React from 'react';
import styled, { css } from 'styled-components';

const { Header } = Layout;

const sizeChooser = (type) => {
  switch (type) {
    case 'sm':
      return 35;
    case 'md':
      return 48;
    case 'lg':
      return 64;
    case 'xl':
      return 72;
    default:
      return 64;
  }
};
const header = ({ children, ...rest }) => (
  <Header {...rest}>
    {children}
    {' '}
  </Header>
);

export const KaoHeader = styled(header)`
  position: ${props => (props.position && props.position) || 'absolute'};
  width: 100%;
  background-color: rgba(30, 29, 50, 0.8);
  box-shadow: 3.5px 6.1px 30px rgba(0, 0, 0, 0.57);
  z-index: 999;
  margin-bottom: 0.0625rem;
  ${props => props.size
    && css`
      height: ${sizeChooser(props.size)}px !important;
      line-height: ${sizeChooser(props.size)}px !important;
    `}
  ${props => props.fixed
    && css`
      position: fixed;
      width: 100%;
    `}
  ${KaoMedia.phonexxs`
    padding: 0 5px !important;
  `};
  ${KaoMedia.phonexs`
    padding: 0 !important;
  `};
  ${KaoMedia.phonesm`
    padding: 0 !important;
  `};
`;
