import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import _ from 'lodash';
import { Menu, Icon } from 'antd';

const { SubMenu } = Menu;
export const RootMenu = styled(Menu)`
  border-bottom: none !important;
  margin: 5px 0 0 0 !important;
  position: relative;
  line-height: 57px !important;
  z-index: 1;
`;
export const SubItem = styled(Menu.Item)`
  font-size: 14px;
  color: #323232;
  margin-bottom: 0 !important;
  &:hover {
    background-color: rgb(225, 225, 225);
  }
`;
export const RootSub = styled(SubMenu)`
  &:hover {
    background-color: rgb(225, 225, 225);
  }
`;

const levelMap = {};

const getMenus = (menus, parent = true) => menus.map((menu) => {
  if (menu.children) {
    return (
      <RootSub
        key={menu.key}
        style={menu.styles || { float: 'left' }}
        title={(
          <span>
            {menu.name}
            {!menu.noicon && <Icon type="caret-down" style={{ fontSize: 11, marginLeft: 10 }} />}
          </span>
)}
      >
        {getMenus(menu.children, false)}
      </RootSub>
    );
  }
  if (parent === false) levelMap[menu.key] = menu;
  return (
    <SubItem key={menu.key} style={menu.styles || ''}>
      {menu.name}
    </SubItem>
  );
});

const handleClickMenu = (e, history, items, signOut = null) => {
  const parent = _.find(items, i => i.key === e.key);
  const level = _.find(levelMap, i => i.key === e.key);
  if (e.key === 'logout' && signOut) {
    console.log(e.key); // eslint-disable-line
    signOut()
      .then(resp => history.push('/'))
      .catch(err => console.log(err));
    return;
  }
  if (parent === undefined) {
    if (level === undefined) return;
    history.push(level.path);
    return;
  }
  history.push(parent.path);
};

const KaoMenu = ({
  history, menus = [], mode = 'horizontal', theme = 'light', signOut = null,
}) => {
  const menuitem = getMenus(menus);
  return (
    <RootMenu onClick={e => handleClickMenu(e, history, menus, signOut)} theme={theme} mode={mode}>
      {menuitem}
    </RootMenu>
  );
};

KaoMenu.propTypes = {
  menus: PropTypes.array.isRequired,
  mode: PropTypes.string,
  theme: PropTypes.string,
};

KaoMenu.defaultProps = {
  menus: [],
  mode: 'horizontal',
  theme: 'light',
};

export { KaoMenu };
