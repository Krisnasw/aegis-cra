import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  Input, Button, Table, Modal,
} from 'antd';
import {
  DropOption, KaoRow, KaoCol, KaoSpace, KaoAlert,
} from 'kao-ui';
import Link from 'next/link';

const tableStyled = ({ className, ...rest }) => <Table className={className} {...rest} />;

export const KaoTable = styled(tableStyled)`
  .ant-table-pagination {
    float: none !important;
    display: table;
    margin: 16px auto !important;
  }
`;

const confirm = Modal.confirm;
const Search = Input.Search;

export const ListTable = ({
  link,
  onCreate,
  onDeleteItem,
  onEditItem,
  onSearchItem,
  tablename,
  buttonname,
  columns,
  isSuccess,
  isGroup,
  linkGroup,
  messageSuccess,
  placeholderSearch,
  isAction = true,
  isAdd,
  isButtonDeactivated,
  onClickDeactivated,
  onCloseSuccessAlert = () => {},
  ...tableProps
}) => {
  const handleMenuClick = (record, e) => {
    if (e.key === '1') {
      onEditItem(record);
    } else if (e.key === '2') {
      confirm({
        title: 'Are you sure delete this record?',
        onOk() {
          onDeleteItem(record);
        },
      });
    }
  };
  if (isAction) {
    columns.push({
      title: 'Action',
      key: 'action',
      width: 75,
      render: (text, record) => (
        <DropOption
          onMenuClick={e => handleMenuClick(record, e)}
          menuOptions={[{ key: '1', name: 'Edit' }, { key: '2', name: 'Remove' }]}
        />
      ),
    });
  }

  return (
    <div className="koa">
      <KaoRow>
        <KaoCol sm={24} style={{ fontSize: 24, fontWeight: 500 }}>
          {tablename}
        </KaoCol>
      </KaoRow>
      <KaoSpace size="lg" />
      <KaoRow type="flex" justify="space-between">
        <KaoCol span={14}>
          <Search
            placeholder={placeholderSearch}
            size="large"
            style={{ width: 300 }}
            onChange={onSearchItem}
          />
        </KaoCol>
        {isButtonDeactivated && (
          <KaoCol span={5} style={{ textAlign: 'right' }}>
            <Button
              type="danger"
              ghost
              onClick={onClickDeactivated}
              style={{
                color: '#D64425',
                fontWeight: 500,
                width: '173px',
              }}
            >
              Deactivate
            </Button>
          </KaoCol>
        )}
        <KaoCol span={5} style={{ fontSize: 16, textAlign: 'right' }}>
          {isAdd === 1 ? (
            <Link href={link}>
              <Button type="primary">
                Add
                {buttonname}
              </Button>
            </Link>
          ) : (
            <Button type="primary" style={{ fontWeight: 500, width: '173px' }} onClick={onCreate}>
              Add
              {' '}
              {buttonname}
            </Button>
          )}
        </KaoCol>
      </KaoRow>

      {isSuccess && (
        <KaoRow type="flex" justify="center">
          <KaoAlert
            banner
            closable
            message={messageSuccess}
            type="success"
            onClose={onCloseSuccessAlert}
          />
        </KaoRow>
      )}

      <KaoSpace size="lg" />
      <KaoRow>
        <KaoCol sm={24}>
          <KaoTable
            {...tableProps}
            className="kao-table"
            columns={columns}
            rowKey={record => record.uid || record.id}
            style={{ backgroundColor: '#fff' }}
          />
        </KaoCol>
      </KaoRow>
    </div>
  );
};

ListTable.propTypes = {
  tablename: PropTypes.string,
  onCreate: PropTypes.func,
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  columns: PropTypes.array.isRequired,
};

ListTable.defaultProps = {
  tablename: 'List name',
  onCreate: null,
  onDeleteItem: null,
  onEditItem: null,
  columns: [],
};
