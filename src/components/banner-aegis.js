import React from 'react';
import styled from 'styled-components';
import {
  KaoContainer, KaoRow, KaoCol, KaoCard, KaoBG,
} from '../common';

import BGTurnament from '../assets/bg/bg-turnamen.jpg';
import BGInvoker from '../assets/bg/bg-invoker.png';

export default class extends React.PureComponent {
  render() {
    return (
      <Wrapper>
        <KaoContainer>
          <KaoRow>
            <KaoCol span={24}>
              <BannerTitle key="h1">WHAT IS AE-GIS</BannerTitle>
            </KaoCol>
          </KaoRow>
          <KaoRow>
            <KaoCol span={12}>
              <KaoCard style={{ background: 'transparent', color: '#fff', fontSize: '20px' }}>
                <p>
                  Esports are the next big thing, but no one knows how to learn esports properly,
                  the platform for education does not exist yet, while in Indonesia the people
                  mostly don’t recognize (or don’t want to) esports as a legit thing.
                </p>
                <p>
                  This is why the gap between pro players and casual players are so broad there are
                  no educational videos, there is no platform explaining how to become better, there
                  are only so much you can take from watching replays from professionals.
                </p>
                <p>
                  This is where AE-GIS comes in, we are the first platform in Indonesia to provide
                  what you need to become better, to find out the best practices with minimal effort
                  since we’re partnered with several pro players in Indonesia to find out what truly
                  holding you back from progressing!
                </p>
              </KaoCard>
            </KaoCol>
            <KaoCol span={12}>
              <KaoBG image={BGInvoker} width="100%" height="650px" />
            </KaoCol>
          </KaoRow>
        </KaoContainer>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  background-color: rgb(30, 29, 50);
  color: #fff;
  padding: 28px;
  position: relative;
  &:before {
    background-image: url("${BGTurnament}");
    background-size: cover;
    background-position: center center;
    background-repeat: no-repeat;
    content: '${props => props.before}';
    position: absolute;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    opacity: 0.7;
  }
`;

const BannerTitle = styled.h1`
  color: #fff;
  font-size: 34px;
  font-weight: 600;
  line-height: 1.46;
  text-align: center;
  margin: 0 auto;
  padding: 22px;
  text-transform: uppercase;
`;
