import React from 'react';
import '../assets/banner.css';
import styled from 'styled-components';
import BannerAnim, { Element } from 'rc-banner-anim';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';

import { KaoMedia, KaoFlex, KaoCol } from '../common';
import SlideOne from '../assets/bg/slide1.jpg';
import SlideTwo from '../assets/bg/slide2.jpg';
import SlideThree from '../assets/bg/slide3.jpg';

export default class extends React.Component {
  render() {
    return (
      <KaoFlex justify="space-around" align="middle">
        <KaoCol span={24}>
          <BannerAnim>
            <Element key="0" prefixCls="banner-user-elem">
              <BgElement
                key="bg"
                className="bg"
                style={{
                  backgroundImage: `url(${SlideOne})`,
                  backgroundSize: 'cover',
                  backgroundPosition: 'center center',
                }}
              />
              <QueueAnim ease="easeInOutCirc">
                <BannerTitle key="h1">THE FIRST INDONESIAN E-SPORTS ACADEMY</BannerTitle>
                <FeatureTitle font={42} key="p">
                  WELCOME TO AEGIS
                </FeatureTitle>
              </QueueAnim>
              <QueueAnim ease="easeInOutBack">
                <FeatureTitle font={22} key="p">
                  Aegis the first educational e-sports in indonesia we present to you e-sports
                  gaming coach platform, fast learning from the pro's in the game get better in just
                  one modul and do your best practice We choose dota 2 from many other esports games
                  to become the first aegis program We think dota is a very competitive game and the
                  level of difficulty is high because of that we choose dota 2
                </FeatureTitle>
              </QueueAnim>
            </Element>
            <Element key="1" prefixCls="banner-user-elem">
              <BgElement
                key="bg"
                className="bg"
                style={{
                  backgroundImage: `url(${SlideTwo})`,
                  backgroundSize: 'cover',
                  backgroundPosition: 'center center',
                }}
              />
              <QueueAnim ease="easeInOutCirc">
                <BannerTitle key="h1">The first aegis esports program</BannerTitle>
                <FeatureTitle font={48} key="p">
                  DOTA 2
                </FeatureTitle>
              </QueueAnim>
              <QueueAnim ease="easeInOutBack">
                <FeatureTitle font={22} key="p">
                  We choose dota 2 from many other esports games to become the first aegis program
                  We think dota is a very competitive game and the level of difficulty is high
                  because of that we choose dota 2
                </FeatureTitle>
              </QueueAnim>
            </Element>
            <Element key="2" prefixCls="banner-user-elem">
              <BgElement
                key="bg"
                className="bg"
                style={{
                  backgroundImage: `url(${SlideThree})`,
                  backgroundSize: 'cover',
                  backgroundPosition: 'center center',
                }}
              />
              <QueueAnim ease="easeInOutCirc">
                <BannerTitle key="h1">A Selection of the Best</BannerTitle>
                <FeatureTitle font={48} key="p">
                  COACH JUST FOR YOU
                </FeatureTitle>
              </QueueAnim>
              <QueueAnim ease="easeInOutBack">
                <FeatureTitle font={22} key="p">
                  We provide you the best trainers or coach Who have player professionals in their
                  fields and the best in teaching you
                </FeatureTitle>
              </QueueAnim>
            </Element>
          </BannerAnim>
          <BannerWrapper>
            <BannerMouse>
              <TweenOne
                animation={{
                  y: '=8',
                  yoyo: true,
                  repeat: -1,
                  duration: 1000,
                }}
                className="banner0-icon"
                key="icon"
              >
                <MouseBar />
              </TweenOne>
            </BannerMouse>
          </BannerWrapper>
        </KaoCol>
      </KaoFlex>
    );
  }
}

const BgElement = Element.BgElement;

const BannerWrapper = styled.div`
  position: absolute;
  bottom: 60px;
  left: 0;
  right: 0;
  color: #fff;
  font-size: 32px;
  text-align: center;
`;

const MouseBar = styled.div`
  width: 4px;
  height: 12px;
  border-radius: 2px;
  background: #fff;
  margin: 5px auto auto;
`;

const BannerMouse = styled.div`
  width: 28px;
  height: 42px;
  border-radius: 14px;
  border: 2px solid #fff;
  margin: auto;
`;

const BannerTitle = styled.h1`
  color: #fff;
  font-size: 34px;
  font-weight: 600;
  line-height: 1.46;
  text-align: center;
  margin: 0 auto;
  padding-top: ${150 / 16}rem;
  text-transform: uppercase;
`;

const FeatureTitle = styled.p`
  color: #fff;
  font-size: ${props => (props.font && props.font) || 26}px;
  font-weight: 300;
  line-height: 1.46;
  text-align: center;
  margin: 0 auto;
  padding-top: ${75 / 16}rem;
  ${KaoMedia.phonesm`
  width: 100%;
`};
  ${KaoMedia.tablet`
  width: 630px;
`};
  ${KaoMedia.desktop`
  width: 630px;
`};
  ${KaoMedia.retina`
  width: 630px;
`};
  ${KaoMedia.retina4K`
  width: 630px;
`};
`;
