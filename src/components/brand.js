import React from 'react';
import styled from 'styled-components';
import { Col } from 'antd';
import QueueAnim from 'rc-queue-anim';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';
import '../assets/brand.less';

import { KaoFlex, KaoCol } from '../common';

import Discord from '../assets/images/discord-logo.png';
import Eblue from '../assets/images/eblue-logo.png';
import Gigabyte from '../assets/images/gigabyte-logo.png';
import Rubix from '../assets/images/rubix-logo.png';

const getBlock = data => ({
  name: data.name,
  className: 'block',
  md: 8,
  xs: 24,
  children: {
    wrapper: {
      className: 'block-content',
    },
    img: {
      children: data.img,
    },
  },
});

const BlockData = [
  getBlock({
    name: 'block0',
    img: `${Rubix}`,
  }),
  getBlock({
    name: 'block1',
    img: `${Discord}`,
  }),
  getBlock({
    name: 'block2',
    img: `${Eblue}`,
  }),
  getBlock({
    name: 'block3',
    img: `${Gigabyte}`,
  }),
];
class Brand extends React.Component {
  getChildrenToRender = data => data.map(item => (
    <Col
      {...item}
      key={item.name}
      data-edit="Col"
    >
      <div {...item.children.wrapper}>
        <span {...item.children.img}>
          <img src={item.children.img.children} alt="img" />
        </span>
      </div>
    </Col>
  ));

  render() {
    const childrenToRender = this.getChildrenToRender(BlockData);
    return (
      <Wrapper>
        <KaoFlex justify="space-around" align="middle">
            <KaoCol span={24}>
              <div className="brand-wrapper">
                <div className="brand">
                  <div key="title" className="title-wrapper" data-edit="titleWrapper">
                    <div className="title-image">
                      <img src="https://gw.alipayobjects.com/zos/rmsportal/PiqyziYmvbgAudYfhuBr.svg" />
                    </div>
                  </div>
                  <OverPack className="img-wrapper" playScale={0.3}>
                    <QueueAnim ease="easeOutQuad">
                        {childrenToRender}
                    </QueueAnim>
                  </OverPack>
                </div>
              </div>
            </KaoCol>
        </KaoFlex>
      </Wrapper>
    );
  }
}

export default Brand;

const Wrapper = styled.div`
  color: #fff;
  position: relative;
`;
