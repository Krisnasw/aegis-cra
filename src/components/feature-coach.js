import React from 'react';
import styled from 'styled-components';
import { Card } from 'antd';
import { KaoContainer, KaoFlex, KaoRow, KaoCol } from '../common';

import QueueAnim from 'rc-queue-anim';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';
import TweenOne from 'rc-tween-one';

import '../assets/feature-coach.css';
import TeamAufa from '../assets/teams/aufa.jpg';
import TeamDestati from '../assets/teams/destati.jpg';
import TeamAmi from '../assets/teams/rocketman.jpg';
import TeamTrio from '../assets/teams/alwaysempath.jpg';
import LineFooter from '../assets/bg/line900.gif';

export default class extends React.Component {
  render() {
    return (
      <Wrapper>
        <LineKontainer>
          <KaoRow>
            <KaoCol span={24}>
              <BannerTitle key="h1">FEATURED COACH</BannerTitle>
            </KaoCol>
          </KaoRow>
          <OverPack className="img-wrapper" playScale={0.3}>
            <QueueAnim type="bottom" ease="easeOutQuad">
              <KaoFlex justify="space-around" align="middle">
                <TweenOne
                    span={4}
                    component={KaoCol}
                    animation={{y: 30, opacity: 0, type: 'from', ease: 'easeOutQuad', delay: 800 }}
                    key="0"
                >
                  <TeamCard className="blog-card" bordered={false} hoverable image={TeamAufa}>
                    <div className="title-content">
                      <h3>Aufa</h3>
                      <hr />
                      <div className="intro">"FLIPFLOP"</div>
                    </div>
                    <div className="utility-info">
                      <ul className="utility-list">
                        <li className="comments">7100 MMR</li>
                        <li className="date">OFFLANE</li>
                      </ul>
                    </div>
                  </TeamCard>
                </TweenOne>
                <TweenOne
                    span={4}
                    component={KaoCol}
                    animation={{y: 30, opacity: 0, type: 'from', ease: 'easeOutQuad', delay: 910 }}
                    key="1"
                >
                  <TeamCard className="blog-card" bordered={false} hoverable image={TeamTrio}>
                    <div className="title-content">
                      <h3>Trio P</h3>
                      <hr />
                      <div className="intro">"ALWAYSEMPATH"</div>
                    </div>
                    <div className="utility-info">
                      <ul className="utility-list">
                        <li className="comments">7024 MMR</li>
                        <li className="date">SUPPORT</li>
                      </ul>
                    </div>
                  </TeamCard>
                </TweenOne>
                <TweenOne
                    span={4}
                    component={KaoCol}
                    animation={{y: 30, opacity: 0, type: 'from', ease: 'easeOutQuad', delay: 1010 }}
                    key="2"
                >
                  <TeamCard className="blog-card" bordered={false} hoverable image={TeamAmi}>
                    <div className="title-content">
                      <h3>Ami</h3>
                      <hr />
                      <div className="intro">"ROCKETMAN"</div>
                    </div>
                    <div className="utility-info">
                      <ul className="utility-list">
                        <li className="comments">6300 MMR</li>
                        <li className="date">SUPPORT</li>
                      </ul>
                    </div>
                  </TeamCard>
                </TweenOne>
                <TweenOne
                    span={4}
                    component={KaoCol}
                    animation={{y: 30, opacity: 0, type: 'from', ease: 'easeOutQuad', delay: 1210 }}
                    key="3"
                >
                  <TeamCard className="blog-card" bordered={false} hoverable image={TeamDestati}>
                    <div className="title-content">
                      <h3>Iddo</h3>
                      <hr />
                      <div className="intro">"DESTATI"</div>
                    </div>
                    <div className="utility-info">
                      <ul className="utility-list">
                        <li className="comments">6000 MMR</li>
                        <li className="date">OFFLANE</li>
                      </ul>
                    </div>
                  </TeamCard>
                </TweenOne>
              </KaoFlex>
            </QueueAnim>
          </OverPack>
        </LineKontainer>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  background-color: rgb(30, 29, 50);
  color: #fff;
  padding: 28px;
  position: relative;
`;

const LineKontainer = styled(KaoContainer)`
  background-image: url("${LineFooter}");
  background-repeat: repeat-x;
  background-position: left bottom;
  padding: 18px 10px;
`;

const TeamCard = styled(Card)`
    background-color: rgb(30, 29, 50);
    color: #fff;
    position: relative;
    width: 130%;
    height: 350px;
    &:before {
        background-image: url("${props => props.image}");
        background-size: cover;
        background-position: center center;
        background-repeat: no-repeat;
        content: '${props => props.before}';
        position: absolute;
        left: 0;
        top: 0;
        right: 0;
        bottom: 0;
        opacity: 0.6;
    }
`;

const BannerTitle = styled.h1`
  color: #fff;
  font-size: 34px;
  font-weight: 600;
  line-height: 1.46;
  text-align: center;
  margin: 0 auto;
  padding: 22px;
  text-transform: uppercase;
`;
