import React from 'react';
import styled from 'styled-components';
import { Breadcrumb } from 'antd';
import {
  KaoContainer, KaoRow, KaoCol, KaoBG,
} from '../common';
import BgCover from '../assets/bg/footer.jpg';
import LogoAegis from '../assets/images/aegis-logo.png';
import IGLogo from '../assets/images/ig-logo.png';
import FBLogo from '../assets/images/fb-logo.png';
import LineFooter from '../assets/bg/line900.gif';

class Footer extends React.PureComponent {
  render() {    
    return (
      <Wrapper>
        <LineKontainer>
          <KaoRow>
            <KaoCol>
              <BreadMenu separator="//">
                <Breadcrumb.Item>Privacy Policy</Breadcrumb.Item>
                <Breadcrumb.Item href="">Become a Coach</Breadcrumb.Item>
                <Breadcrumb.Item href="">Terms of Service</Breadcrumb.Item>
                <Breadcrumb.Item>Our Team</Breadcrumb.Item>
                <Breadcrumb.Item>Contact Us</Breadcrumb.Item>
              </BreadMenu>
            </KaoCol>
          </KaoRow>
          <KaoRow style={{padding: '85px 18px'}}>
            <KaoCol md={12}>
              <KaoBG image={LogoAegis} width="215px" height="65px" />
            </KaoCol>
            <KaoCol md={12}>
              <KaoBG image={IGLogo} width="45px" height="45px" style={{float: 'right', margin: '15px'}} />
              <KaoBG image={FBLogo} width="75px" height="75px" style={{float: 'right'}} />
            </KaoCol>
          </KaoRow>
          <FooterNote>
            <KaoCol md={12}>
            <p>
              <a href="mailto:info@bristolarchiverecords.com">
                <img src="img/foot/email.gif" alt="" width="11" height="8"/>
                info@aegis.co.id
              </a>
              <br/>
              <a href="#">© Aegis World 2018</a>&nbsp;//&nbsp;
                The First Educational gaming platform in Indonesia
            </p>
            </KaoCol>
            <KaoCol md={12} style={{
              textAlign: 'right'
            }}>
              <p>
                <strong>Created by ESports Player</strong>
              <br/>
              <a href="http://www.flickr.com/photos/41176059@N03/" target="_blank"><strong>&nbsp;»&nbsp;Instagram</strong></a>
              <a href="http://www.myspace.com/bristolarchiverecordsuk" target="_blank"><strong>&nbsp;»&nbsp;Facebook </strong></a>
              </p>
            </KaoCol>
          </FooterNote>
        </LineKontainer>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  background-color: rgb(30, 29, 50);
  color: #fff;
  font-size: 12px;
  line-height: 18px;
  min-height: 170px;
  position: relative;
  &:before {
    background-image: url("${BgCover}");
    background-size: cover;
    background-position: center center;
    background-repeat: no-repeat;
    content: '${props => props.before}';
    position: absolute;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    opacity: 0.3;
  }
  > a {
    color: #fff !important;
  }
`;

const LineKontainer = styled(KaoContainer)`
  background-image: url("${LineFooter}");
  background-repeat: repeat-x;
  background-position: left top;
  padding: 18px 10px;
`;

const BreadMenu = styled(Breadcrumb)`
  .ant-breadcrumb-link {
    color: #fff;
  }

  .ant-breadcrumb-separator {
    color: #f44242;
  }
`;

const FooterNote = styled(KaoRow)`
  color: #fff !important;
  position: relative;
  bottom: 0;
  a {
    color: #fff;
  }
`;
export default Footer;
