import React from 'react';
import styled from 'styled-components';

import BannerAnim from 'rc-banner-anim';
import QueueAnim from 'rc-queue-anim';
import { TweenOneGroup } from 'rc-tween-one';
import Icon from 'antd/lib/icon';
import PropTypes from 'prop-types';

import { KaoContainer, KaoRow, KaoCol } from '../common';

import '../assets/testimonials.css';
import LineFooter from '../assets/bg/line900.gif';
import RRQ from '../assets/bg/RRQ.png';
import PGBarracx from '../assets/bg/pg-barrack.png';

import RAMZ from '../assets/images/ramz-pg.jpg';
import YABYOO from '../assets/images/yabyoo.jpg';
import VINZ from '../assets/images/vinz-pg.jpg';


const dataArray = [
  {
    pic: `${RAMZ}`,
    map: `${PGBarracx}`,
    color: '#2b0119',
    background: 'rgb(30, 29, 50)',
    content:
        'Dengan adanya ae-gis ini mungkin akan mengembangkan scene dota 2 di indonesia yang akan berperan sebagai wadah untuk player yang ingin menjadi pro player.',
    title: 'Ramz - Midlanner',
  },
  {
    pic: `${YABYOO}`,
    map: `${RRQ}`,
    color: '#161616',
    background: 'rgb(30, 29, 50)',
    content:
        'Sebagai pemain dota 2 yang berasal dari surabaya pengalaman gw itu player competitive di surabaya masih sedikit , dengan adanya ae-gis akan mengembangkan scene dota 2 di surabaya  yang punya jiwa berkompetensi.',
    title: 'YabYoo - Midlanner',
  },
  {
    pic: `${VINZ}`,
    map: `${PGBarracx}`,
    color: '#2b0119',
    background: 'rgb(30, 29, 50)',
    content:
        'Fatback landjaeger prosciutto jerky ham. Salami cappig cupim leberkas. Kevin bacon shankle tenderloin ground round.',
    title: 'Vinz - Godlike Player',
  },
];

export default class extends React.Component {
  static propTypes = {
    className: PropTypes.string,
  };

  static defaultProps = {
    className: 'testimonial-slide',
  };

  constructor(props) {
    super(props);
    this.state = {
      showInt: 0,
      delay: 0,
      imgAnim: [
        { translateX: [0, 300], opacity: [1, 0] },
        { translateX: [0, -300], opacity: [1, 0] },
      ],
    };
    this.oneEnter = false;
  }

  onChange = () => {
    if (!this.oneEnter) {
      this.setState({ delay: 300 });
      this.oneEnter = true;
    }
  };

  onLeft = () => {
    let showInt = this.state.showInt;
    showInt -= 1;
    const imgAnim = [
      { translateX: [0, -300], opacity: [1, 0] },
      { translateX: [0, 300], opacity: [1, 0] },
    ];
    if (showInt <= 0) {
      showInt = 0;
    }
    this.setState({ showInt, imgAnim });
    this.bannerImg.prev();
    this.bannerText.prev();
  };

  onRight = () => {
    let showInt = this.state.showInt;
    const imgAnim = [
      { translateX: [0, 300], opacity: [1, 0] },
      { translateX: [0, -300], opacity: [1, 0] },
    ];
    showInt += 1;
    if (showInt > dataArray.length - 1) {
      showInt = dataArray.length - 1;
    }
    this.setState({ showInt, imgAnim });
    this.bannerImg.next();
    this.bannerText.next();
  };

  getDuration = (e) => {
    if (e.key === 'map') {
      return 800;
    }
    return 1000;
  };

  render() {
    const imgChildren = dataArray.map((item, i) => (
      <Element key={i} style={{ background: item.color }} leaveChildHide>
        <QueueAnim
          animConfig={this.state.imgAnim}
          duration={this.getDuration}
          delay={[!i ? this.state.delay : 300, 0]}
          ease={['easeOutCubic', 'easeInQuad']}
          key="img-wrapper"
        >
          <div className={`${this.props.className}-map map${i}`} key="map">
            <img src={item.map} width="100%" />
          </div>
          <div className={`${this.props.className}-pic pic${i}`} key="pic">
            <img src={item.pic} width="100%" />
          </div>
        </QueueAnim>
      </Element>
    ));
    const textChildren = dataArray.map((item, i) => {
      const { title, content, background } = item;
      return (
        <Element key={i}>
          <QueueAnim type="bottom" duration={1000} delay={[!i ? this.state.delay + 500 : 800, 0]}>
            <h1 key="h1">{title}</h1>
            <em key="em" style={{ background }} />
            <p key="p">{content}</p>
          </QueueAnim>
        </Element>
      );
    });
    return (
      <Wrapper>
        <LineKontainer>
          <KaoRow>
            <KaoCol span={24}>
              <BannerTitle key="h1">WHAT PLAYER ARE SAYING</BannerTitle>
            </KaoCol>
          </KaoRow>
          <KaoRow>
            <KaoCol span={24}>
              <div
                className={`${this.props.className}-wrapper`}
                style={{ background: dataArray[this.state.showInt].background }}
              >
                <div className={this.props.className}>
                  <BannerAnim
                    prefixCls={`${this.props.className}-img-wrapper`}
                    sync
                    type="across"
                    duration={1000}
                    ease="easeInOutExpo"
                    arrow={false}
                    thumb={false}
                    ref={(c) => {
                      this.bannerImg = c;
                    }}
                    onChange={this.onChange}
                    dragPlay={false}
                  >
                    {imgChildren}
                  </BannerAnim>
                  <BannerAnim
                    prefixCls={`${this.props.className}-text-wrapper`}
                    sync
                    type="across"
                    duration={1000}
                    arrow={false}
                    thumb={false}
                    ease="easeInOutExpo"
                    ref={(c) => {
                      this.bannerText = c;
                    }}
                    dragPlay={false}
                  >
                    {textChildren}
                  </BannerAnim>
                  <TweenOneGroup enter={{ opacity: 0, type: 'from' }} leave={{ opacity: 0 }}>
                    {this.state.showInt && <Icon type="left" key="left" onClick={this.onLeft} />}
                    {this.state.showInt < dataArray.length - 1 && (
                      <Icon type="right" key="right" onClick={this.onRight} />
                    )}
                  </TweenOneGroup>
                </div>
              </div>
            </KaoCol>
          </KaoRow>
        </LineKontainer>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  background-color: rgb(30, 29, 50);
  color: #fff;
  padding: 28px;
  position: relative;
`;

const LineKontainer = styled(KaoContainer)`
  background-image: url("${LineFooter}");
  background-repeat: repeat-x;
  background-position: left bottom;
  padding: 18px 0px;
`;

const BannerTitle = styled.h1`
  color: #fff;
  font-size: 34px;
  font-weight: 600;
  line-height: 1.46;
  text-align: center;
  margin: 0 auto;
  padding: 22px;
  text-transform: uppercase;
`;

const Element = BannerAnim.Element;
